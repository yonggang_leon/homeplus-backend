package com.homeplus.googleMapsApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class GeocodeApi {

 public Integer getDistanceBetweenTasks(String[] address, String[] address2) throws IOException, InterruptedException, ApiException {
     String api_key = "AIzaSyDqbnjFcY4yyRcIpDc7hqB4sL1vDtdwU8I";
     GeoApiContext context = new GeoApiContext.Builder()
             .apiKey(api_key)
             .build();

     DistanceMatrix distanceResults = DistanceMatrixApi.getDistanceMatrix(context, address, address2).await();

     Integer distance = Math.toIntExact(distanceResults.rows[0].elements[0].distance.inMeters);

     context.shutdown();
        try {
            return distance;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
 }

}
