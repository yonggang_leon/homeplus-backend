package com.homeplus.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "task_post")
public class TaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private UserEntity userEntity;

    @ManyToOne
    @JoinColumn(name = "tasker_id", referencedColumnName = "tasker_id")
    private TaskerEntity taskerEntity;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "category", nullable = false)
    private String category;

    @Column(name = "date", nullable = false)
    private OffsetDateTime date;

    @ManyToOne
    @JoinColumn(name = "time_section", referencedColumnName = "time_section_id")
    private TimeSectionsEntity timeSectionsEntity;

    @Column(name = "budget", nullable = false)
    private Long budget;

    @Column(name = "price")
    private String price;

    @Column(name = "state")
    private String state;

    @Column(name = "suburb")
    private String suburb;

    @Column(name = "street")
    private String street;

    @Column(name = "postcode")
    private Long postcode;

    @Column(name = "house_type")
    private String house_type;

    @Column(name = "num_of_rooms")
    private Integer num_of_rooms;

    @Column(name = "num_of_bathrooms")
    private Integer num_of_bathrooms;

    @Column(name = "levels")
    private Integer levels;

    @Column(name = "lift")
    private Boolean lift;

    @Column(name = "in_person")
    private Boolean in_person;

    @Column(name = "item_name")
    private String item_name;

    @Column(name = "item_value")
    private String item_value;

    @Column(name = "description")
    private String description;

    @Column(name = "task_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TaskStatus task_status;

    @Column(name = "created_time", nullable = false)
    private OffsetDateTime created_time;

    @Column(name = "updated_time")
    private OffsetDateTime updated_time;

}
