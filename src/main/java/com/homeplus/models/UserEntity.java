package com.homeplus.models;

import lombok.*;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Date;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_info")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String password;

    @Column
    private String gender;

    @Column
    private String language;

    @Column
    private String state;

    @Column
    private String street;

    @Column
    private String postcode;

    @Column
    private Date date_of_birth;

    @Column
    private Integer mobile;

    @Column
    private Boolean is_tasker;

    @Column
    private Boolean is_tasker_data;

    @Column
    private String status;

    @Column(name = "createdTime", nullable = false)
    private OffsetDateTime createdTime;

    @Column(name = "updatedTime", nullable = false)
    private OffsetDateTime updatedTime;


}
