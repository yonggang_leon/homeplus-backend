package com.homeplus.controllers;

import com.homeplus.dtos.taskerReview.TaskReviewGetDto;
import com.homeplus.dtos.taskerReview.TaskReviewPostDto;
import com.homeplus.services.TaskReviewService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
public class TaskReviewController {

    private final TaskReviewService taskReviewService;

    @GetMapping("tasker-reviews")
    public ResponseEntity<List<TaskReviewGetDto>> findByTasker(@RequestParam Long id) {
        return ResponseEntity.ok(taskReviewService.getReviewsByTasker(id));
    }

    @PostMapping("create-review")
    public ResponseEntity createReview(@Valid @RequestBody TaskReviewPostDto taskReviewPostDto) {
        taskReviewService.createReview(taskReviewPostDto);
        return ResponseEntity.ok("success");
    }
}
